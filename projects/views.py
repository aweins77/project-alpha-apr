from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }

    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }

    return render(request, "projects/detail.html", context)


@login_required
def project_create(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()

            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
